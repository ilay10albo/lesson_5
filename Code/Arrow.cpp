#include "Arrow.h"

 double Arrow::getArea() const
{
	 return 0;
}

 double Arrow::getPerimeter() const
 {
	  return abs(_p2.getX() - _p1.getX());
 }

 void Arrow::printDetails() const
 {
	 Shape::printDetails();
	 cout << " Area: " << this->getArea() <<  " Perimeter: " << this->getPerimeter() ;
 }

 void Arrow::move(const Point& other)
 {
	 _p1 += other;
	 _p2 += other;
 }

Arrow::Arrow(const Point& a, const Point& b, const string& name, const string& type) : Shape(name , type)
{
	_p1 = a;
	_p2 = b;
}
void Arrow::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char RED[] = { 255, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), RED, 100.0f).display(disp);
}
void Arrow::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), BLACK, 100.0f).display(disp);
}


Arrow::~Arrow()
{

}