#include "Circle.h"


Circle::Circle(const Point & center, double radius, const string & name, const string & type) : Shape(name,type)
{
	this->_center = center;
	this->_radios = radius;
}


 void Circle::move(const Point& other)
{
	 this->_center += other;
}
Circle::~Circle()
{
}

const Point & Circle::getCenter() const
{
	return _center;
	
}

double Circle::getRadius() const
{
	return  this->_radios;
}

void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLUE[] = { 0, 0, 255 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLUE, 100.0f).display(disp);	
}

void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLACK, 100.0f).display(disp);
}

void Circle::printDetails()
{
	cout << "Point: " << _center.getX() << "," << _center.getY() << " radius: " << _radios;
}

double Circle::getArea() const
{
	return this->_radios*this->_radios*PI;
}

double Circle::getPerimeter() const
{
	return 2*PI*this->_radios;
}


