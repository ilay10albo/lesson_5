#include "Triangle.h"

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const string& name, const string& type) : Polygon(name , type)
{
	
	if ((a.getX() == b.getX()  || a.getX()  == c.getX()) || (a.getY() == b.getY() || a.getY() == c.getY()))
	{

		cout << "[!] Can't use same points with same y or x";
	}
	else
	{
		_points.push_back(Point(a.getX(), a.getY())); // Insert point
		_points.push_back(Point(b.getX(), b.getY())); // Insert point
		_points.push_back(Point(c.getX(), c.getY())); // Insert point
	}
}

void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}
void Triangle::printDetails() const
{

	cout << "\n";
	Shape::printDetails();
	cout << " Param: " << getPerimeter() << " Area: " << getArea();

}
double Triangle::getPerimeter() const
{
	return _points[0].distance(_points[1]) + _points[1].distance(_points[2]) + _points[2].distance(_points[0]); // Calculate permeter
}
double Triangle::getArea() const
{
	return ((_points[0].getX()*(_points[1].getY()-_points[2].getY()) + _points[1].getX()*(_points[2].getY()- _points[0].getY()) + _points[2].getX()*(_points[0].getY() - _points[1].getY()))/2); // Calculate area
}

Triangle::~Triangle()
{

}