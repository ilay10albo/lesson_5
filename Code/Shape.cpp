#include "Shape.h"
using  namespace std;

Shape::Shape(const string& name, const string& type)
{
	this->_name = name;
	this->_type = type;
}

void Shape::printDetails() const
{
	cout << "Name: "<<_name.c_str() << ", Type: "<< _type.c_str() << "\n";
}

string Shape::getType() const
{
	return this->_type;
}
string Shape::getName() const
{
	return this->_name;
}