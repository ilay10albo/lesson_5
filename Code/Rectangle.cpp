#include "Rectangle.h"


double MyRectangle::getPerimeter() const
{
	return (_points[1].getX() - _points[0].getX()) * 2 + (_points[0].getY() - _points[1].getY());
}
double MyRectangle::getArea() const
{
	return(_points[1].getX() - _points[0].getX())*(_points[0].getY() - _points[1].getY());
}

void MyRectangle::printDetails() const
{
	cout << "\n";
	Shape::printDetails();
	cout << " Param: " << getPerimeter() << " Area: " << getArea();
}

MyRectangle::MyRectangle(const Point & a, double length, double width, const string & type, const string & name) : Polygon(type, name)
{
	if (width <= 0 || length <=0)
	{
		std::cout << "[!] Can't have a zero argument";
	}
	else
	{
		_points.push_back(Point(a)); // Insert point
		_points.push_back(Point(a.getX() + width, a.getY() + length)); // Insert point
	}
	
}

void MyRectangle::MyRectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
}



void MyRectangle::MyRectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0};
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}


MyRectangle::~MyRectangle()
{

}