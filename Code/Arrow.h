#pragma once
#include "Polygon.h"

class Arrow : public Shape
{
public:
	Arrow(const Point& a, const Point& b, const string& name, const string& type);
	~Arrow();

	virtual void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	virtual void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	virtual void move(const Point& other);
	virtual double getArea() const;
	virtual double getPerimeter() const;
	void printDetails() const;
	// override functions if need (virtual + pure virtual)

private:
	Point _p1;
	Point _p2;
};