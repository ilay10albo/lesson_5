#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>


class Menu
{
public:

	Menu();
	~Menu();
	void printMenu(); // Prints menu
	void printShapeMenu(); // Prints shape menu 
	int circleController(); // Responsible for making new circle
	int  trinagleController(); // Responsible for making new trinagle
	int arrowController(); // Responsible for making new arrow
	int rectengleController(); // Responsible for making new rectengle
	void manageEditor(); // Main menu loop action bar 
	void shapeEditor(); // Manage the menu of adding new shapes 
	void deleteAll(); // Deletes all shapes
	int ajust(); // Manages the ajust shapes menu (Change location , delete etc...)
	void ajustMenu(); // Prints the menu itself
	void update(); // Updates the screen
	void movShape(int index); // Move shape to new location
	void deleteSingleShape(int index); // Deletes spesific shape

private: 

	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
	vector<Shape*> _shapes;
	

};

