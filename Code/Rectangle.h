#pragma once
#include "Polygon.h"
	// Calling it MyRectangle becuase Rectangle is taken by global namespace.
class MyRectangle : public Polygon
{
	public:
		// There's a need only for the top left corner 
		MyRectangle(const Point& a, double length, double width, const string& name, const string& type);
		virtual ~MyRectangle();
		// override functions if need (virtual + pure virtual)
		virtual void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
		virtual void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
		virtual double getPerimeter() const; // Gets peramiter
		virtual double getArea() const; // Gets area
		void printDetails() const;
};