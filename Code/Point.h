#pragma once
#include <vector>
//#include "CImg.h"
#include <iostream>
using namespace std;
class Point
{
private:
	int _x;
	int _y;
public:
	Point();
	Point(double x, double y);
	Point(const Point& other);
	virtual ~Point();
	
	Point operator+(const Point& other) const;
	Point& operator+=(const Point& other);

	double getX() const; // Get x 
	double getY() const; // Get y

	double distance(const Point& other) const; // Calculate distance 

	
};