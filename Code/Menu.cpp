#include "Menu.h"

Menu::Menu() 
{
	_board  = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
	manageEditor();

		
}

void Menu::manageEditor()
{
	int result = 0;
	while (result!=3)
	{
		system("cls");
		printMenu();
		cin >> result;
		getchar();
		switch (result)
		{
		case 0: // Add new shape
			shapeEditor();
			break;
		case 1: // Ajust one of the shapes
			cout << "1";
			ajust();
			break;
		case 2:
			cout << "2";
			deleteAll(); // Delete all
			break;
		case 3: // Exit
			cout << "EXIT";
			break;
		default:
			break;
		} //Clean buffer
	}
}

void Menu::shapeEditor()
{
	int shapeChoise = 0;
	system("cls");
	printShapeMenu();
	cin >> shapeChoise; getchar();
	while (shapeChoise>3 || shapeChoise<0) // Confirm valid choise
	{
		system("cls");
		printShapeMenu();
		cin >> shapeChoise; getchar();
	}
	switch (shapeChoise)
	{
	case 0:
		circleController();
		break;
	case 1:
		arrowController();
		break;
	case 2:
		trinagleController();
		break;
	case 3:
		rectengleController();
		break;
	default:
		break;
	}
}

int Menu::circleController()
{
	double x;
	double y;
	double radius;
	string name;
	cout << "Enter x: ";
	cin >> x; getchar();
	cout << "Enter y: ";
	cin >> y; getchar();
	cout << "Enter radius:";
	cin >> radius; getchar();
	if (radius < 0 || x < 0 || y < 0)   // Valid details validation
	{
		cout << "[!] Can't have negative x , y , or radius";
		return -1;
	}
	cout << "Enter name: ";
	cin >> name; getchar();
	Circle* newC = new Circle(Point(x, y), radius, name,"Circle");
	this->_shapes.push_back(newC);// Add shape
	update();  // Draw shape
	return 0;

}

int Menu::trinagleController()
{
	double x1; double x2; double x3;
	double y1; double y2; double y3;
	
	string name;
	cout << "Enter x1: ";
	cin >> x1; getchar();
	cout << "Enter y1: ";
	cin >> y1; getchar();
	
	cout << "Enter x2: ";
	cin >> x2; getchar();
	cout << "Enter y2: ";
	cin >> y2; getchar();
	
	cout << "Enter x3: ";
	cin >> x3; getchar();
	cout << "Enter y3: ";
	cin >> y3; getchar();
	if (y1 == y2 && y2 == y3) { // Check valid y
		cout << "[!] Can't have a tringale with same y value"; getchar(); return -1;
	}
	else //
	{
		if (x1 < 0 || x2 < 0 || x3 < 0) // Check valid x 
		{
			cout << "[!] Can't have negative x value"; getchar(); return -1;
		}
	}

	cout << "Enter name: ";
	cin >> name; getchar();
	Point a(x1,y1); Point b(x2,y2); Point c(x3,y3); // Declare new points
	Triangle* newS  =  new Triangle(a, b, c, name, "Triangle" );
	_shapes.push_back(newS); // Add shape
	update(); // Draw shape
	return 0;

}

int Menu::arrowController()
{
	int x1 = 0; int y1 = 0;
	int x2 = 0; int y2 = 0;
	string name;
	cout << "Enter x1: ";
	cin >> x1; getchar();
	cout << "Enter y1: ";
	cin >> y1; getchar();

	cout << "Enter x2: ";
	cin >> x2; getchar();
	cout << "Enter y2: ";
	cin >> y2; getchar();
	if (x2 < 0 || y2 < 0|| x1 < 0 || y1 < 0) // Confirm points
	{
		cout << "[!] Can't have negative x , y , or radius"; getchar();
		return -1;
	}

	cout << "Enter name: ";
	cin >> name; getchar();
	Arrow* newArrow = new Arrow(Point(x1, y1), Point(x2, y2), name, "Arrow"); 
	_shapes.push_back(newArrow); // Add shape
	update(); // Draw shape
	return 0;
}

int Menu::rectengleController()
{
	int x = 0; int y = 0;
	int width = 0; int length = 0;
	string name;
	cout << "Enter the X of the to left corner:";
	cin >> x; getchar();
	cout << "Enter the Y of the top left corner:";
	cin >> y; getchar();
	cout << "Please enter the length of the shape:";
	cin >> length; getchar();
	cout <<  "Please enter the width of the shape:";
	cin >> width; getchar();

	cout << "Please enter name of rectangle";
	cin >> name; getchar();
	if (x < 0 || y < 0 || length < 0 || width < 0) // Confirm data not negative
	{
		cout << "[!] Can't have negative values..."; getchar();  return -1;
	}
	MyRectangle *rect = new MyRectangle(Point(x,y) , length , width , name ,"Rectangle");
	_shapes.push_back(rect); // Add shape
	update(); // Draw shape
	return 0;

}


void Menu::deleteAll()
{
	int _currentSize = _shapes.size(); // Save current size
	for (int i = 0; i < _shapes.size(); i++)
	{
		_shapes[i]->clearDraw(*_disp, *_board); // Paint shapes in black
	}
	for (int  i = 0; i < _currentSize; i++)
	{
		delete _shapes[0]; // Delete shape pointer 
		_shapes.erase(_shapes.begin()); // Delete shape from vector
	}
}

int Menu::ajust()
{
	int index = -1;
	int choise = -1;
	system("cls");
	if (!_shapes.size())
	{
		cout << "[!] There are no shapes";
		getchar(); return -1;
	}
	// Get index of shape
	while (index > _shapes.size()-1 || index<0 ) // Valid index ?
	{
		system("cls");
		for (int i = 0; i < _shapes.size(); i++)
		{
			cout << i << ".";
			_shapes[i]->printDetails();
		}
		cout << "\n";
		cin >> index;  getchar();
	}
	// Get action to perform on shape
	while (choise > 2 || choise < 0) // Valid choice?
	{
		system("cls");
		ajustMenu();
		cin >> choise; getchar();
	}
	switch (choise) // Act according to choice
	{
	case 0:
		movShape(index); // Move shape
		break;
	case 1:
		_shapes[index]->printDetails(); // Print details of selected shapes
		cout << "Press any key to continue . . ."; getchar();
		break;
	case 2:
		deleteSingleShape(index); // Delete shape
		break;
	}
	return 0;
}



void Menu::update()
{
	this->_shapes[this->_shapes.size() - 1]->draw(*_disp, *_board); // Draw last added shape
}

void Menu::movShape(int index)
{
	int newXDelta = -1; int newYDelta = -1; // Rest new points
	while (newXDelta < 0 || newYDelta < 0)
	{
		system("cls");
		cout << "Please enter the X moving scale:";
		cin >> newXDelta; getchar(); // Get new x change 
		cout << "Please enter the Y moving scale:";
		cin >> newYDelta; getchar(); // Get new y change
	}
	_shapes[index]->clearDraw(*_disp ,*_board); // Remove shape from board
	_shapes[index]->move(Point(newXDelta, newYDelta)); // Add new values
	for (int i = 0; i < _shapes.size(); i++) // Draw all shapes again
	{
		_shapes[i]->draw(*_disp, *_board);
	}
}

void Menu::deleteSingleShape(int index)
{
	_shapes[index]->clearDraw(*_disp, *_board); // Delete from screen 
	delete _shapes[index]; // Delete from memory 
	_shapes.erase(_shapes.begin() + index); // Delete from vector
	for (int i = 0; i < _shapes.size(); i++) // Draw again
	{
		_shapes[i]->draw(*_disp, *_board);
	}
}

void Menu::ajustMenu()
{
	cout << "Enter 0 to move the shape\n";
	cout << "Enter 1 to get its details.\n";
	cout << "Enter 2 to remove the shape.\n";
}
void Menu::printMenu()
{
	cout << "Enter 0 to add a new shape.\n";
	cout << "Enter 1 to modify or get information from a current shape.\n";
	cout << "Enter 2 to delete all of the shapes.\n";
	cout << "Enter 3 to exit.\n";
}

void Menu::printShapeMenu()
{
	cout << "Enter 0 to add a circle.\n";
	cout << "Enter 1 to add an arrow.\n";
	cout << "Enter 2 to add a triangle.\n";
	cout << "Enter 3 to add a rectangle.\n";
}


Menu::~Menu()
{
	deleteAll(); // Clear memory
	_shapes.clear(); // Clear vector
	_disp->close();
	delete _board;
	delete _disp;
}
