#pragma once
#include "Polygon.h"

class Triangle : public Polygon
{
public:
	Triangle(const Point& a, const Point& b, const Point& c, const string& name, const string& type); // Constractor
	virtual ~Triangle(); 
	virtual double getPerimeter() const; // Gets peramiter
	virtual double getArea() const; // Gets area
	virtual void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	virtual void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	void printDetails() const;
	
};