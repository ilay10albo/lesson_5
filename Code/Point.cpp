#include "Point.h"
Point::Point()
{
	this->_x = 0;
	this->_y = 0;
}

Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}

Point::Point(const Point& other)
{
	this->_x = other.getX();
	this->_y = other.getY();
}
double Point::getX() const
{
	return this->_x;
}
double Point::getY() const
{
	return this->_y;
}
double Point::distance(const Point& other) const
{
	int sum1 = (this->_x - other.getX());
	int sum2 = (this->_y - other.getY());
	double sum3 = sum1 * sum1 + sum2 * sum2;
	return sqrt(sum3);
}

Point Point::operator+(const Point& other) const
{
	return Point(this->_x +other.getX() , this->_y + other.getY());
}
Point& Point::operator+=(const Point& other)
{
	this->_x += other.getX();
	this->_y += other.getY();
	return *this;
}
Point::~Point()
{

}